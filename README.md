# brickdim
[![screenshot](https://gitlab.com/Sdr24/brickdim/-/raw/master/img.png "screenshot")](https://gitlab.com/Sdr24/brickdim/-/raw/master/img.png "screenshot")
## setup
### windows side
install VcXsrv, choose multiple windows, display 0, start no client, disable native opengl   
### wsl side
```console
$ sudo apt install ubuntu-desktop mesa-utils
$ git clone https://gitlab.com/Sdr24/brickdim.git
$ cd brickdim
$ cmake .
$ make
$ export DISPLAY=localhost:0
$ ./brickdim
```
