# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/phil/projects/SimpleRenderEngineTemplate/src/Console.cpp" "/home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o"
  "/home/phil/projects/SimpleRenderEngineTemplate/src/main.cpp" "/home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "SimpleRenderEngine/include"
  "SimpleRenderEngine/submodules/ImGuiColorTextEdit"
  "SimpleRenderEngine/submodules/imgui"
  "SimpleRenderEngine/submodules/glm"
  "SimpleRenderEngine/submodules/picojson"
  "/usr/include/SDL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/phil/projects/SimpleRenderEngineTemplate/SimpleRenderEngine/src/CMakeFiles/SRE.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
