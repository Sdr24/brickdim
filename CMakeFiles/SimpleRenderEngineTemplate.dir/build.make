# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/phil/projects/SimpleRenderEngineTemplate

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/phil/projects/SimpleRenderEngineTemplate

# Include any dependencies generated for this target.
include CMakeFiles/SimpleRenderEngineTemplate.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/SimpleRenderEngineTemplate.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/SimpleRenderEngineTemplate.dir/flags.make

CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o: CMakeFiles/SimpleRenderEngineTemplate.dir/flags.make
CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o: src/Console.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o -c /home/phil/projects/SimpleRenderEngineTemplate/src/Console.cpp

CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/phil/projects/SimpleRenderEngineTemplate/src/Console.cpp > CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.i

CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/phil/projects/SimpleRenderEngineTemplate/src/Console.cpp -o CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.s

CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o: CMakeFiles/SimpleRenderEngineTemplate.dir/flags.make
CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o: src/main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o -c /home/phil/projects/SimpleRenderEngineTemplate/src/main.cpp

CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/phil/projects/SimpleRenderEngineTemplate/src/main.cpp > CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.i

CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/phil/projects/SimpleRenderEngineTemplate/src/main.cpp -o CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.s

# Object files for target SimpleRenderEngineTemplate
SimpleRenderEngineTemplate_OBJECTS = \
"CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o" \
"CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o"

# External object files for target SimpleRenderEngineTemplate
SimpleRenderEngineTemplate_EXTERNAL_OBJECTS =

SimpleRenderEngineTemplate: CMakeFiles/SimpleRenderEngineTemplate.dir/src/Console.cpp.o
SimpleRenderEngineTemplate: CMakeFiles/SimpleRenderEngineTemplate.dir/src/main.cpp.o
SimpleRenderEngineTemplate: CMakeFiles/SimpleRenderEngineTemplate.dir/build.make
SimpleRenderEngineTemplate: SimpleRenderEngine/src/libSRE.a
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libGL.so
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libGLU.so
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libGLEW.so
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libSDL2main.a
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libSDL2.so
SimpleRenderEngineTemplate: /usr/lib/x86_64-linux-gnu/libSDL2_image.so
SimpleRenderEngineTemplate: CMakeFiles/SimpleRenderEngineTemplate.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable SimpleRenderEngineTemplate"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/SimpleRenderEngineTemplate.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/SimpleRenderEngineTemplate.dir/build: SimpleRenderEngineTemplate

.PHONY : CMakeFiles/SimpleRenderEngineTemplate.dir/build

CMakeFiles/SimpleRenderEngineTemplate.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/SimpleRenderEngineTemplate.dir/cmake_clean.cmake
.PHONY : CMakeFiles/SimpleRenderEngineTemplate.dir/clean

CMakeFiles/SimpleRenderEngineTemplate.dir/depend:
	cd /home/phil/projects/SimpleRenderEngineTemplate && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/phil/projects/SimpleRenderEngineTemplate /home/phil/projects/SimpleRenderEngineTemplate /home/phil/projects/SimpleRenderEngineTemplate /home/phil/projects/SimpleRenderEngineTemplate /home/phil/projects/SimpleRenderEngineTemplate/CMakeFiles/SimpleRenderEngineTemplate.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/SimpleRenderEngineTemplate.dir/depend

