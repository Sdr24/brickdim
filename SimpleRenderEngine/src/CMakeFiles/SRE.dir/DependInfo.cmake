# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/phil/projects/brickdim/SimpleRenderEngine/submodules/ImGuiColorTextEdit/TextEditor.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/__/submodules/ImGuiColorTextEdit/TextEditor.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/submodules/imgui/imgui.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/__/submodules/imgui/imgui.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/submodules/imgui/imgui_draw.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/__/submodules/imgui/imgui_draw.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Camera.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Camera.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Color.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Color.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Framebuffer.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Framebuffer.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Inspector.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Inspector.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Light.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Light.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Log.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Log.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Material.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Material.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Mesh.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Mesh.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/ModelImporter.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/ModelImporter.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/RenderPass.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/RenderPass.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Renderer.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Renderer.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Resource.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Resource.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/SDLRenderer.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/SDLRenderer.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Shader.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Shader.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Skybox.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Skybox.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Sprite.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Sprite.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/SpriteAtlas.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/SpriteAtlas.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/SpriteBatch.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/SpriteBatch.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/Texture.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/Texture.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/VR.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/VR.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/WorldLights.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/WorldLights.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/imgui_sre.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/imgui_sre.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/impl/GL.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/impl/GL.cpp.o"
  "/home/phil/projects/brickdim/SimpleRenderEngine/src/sre/impl/UniformSet.cpp" "/home/phil/projects/brickdim/SimpleRenderEngine/src/CMakeFiles/SRE.dir/sre/impl/UniformSet.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "SimpleRenderEngine/include"
  "SimpleRenderEngine/submodules/ImGuiColorTextEdit"
  "SimpleRenderEngine/submodules/imgui"
  "SimpleRenderEngine/submodules/glm"
  "SimpleRenderEngine/submodules/picojson"
  "/usr/include/SDL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
