#include "Console.hpp"
#include <iostream>

void Console::toggle() {
    showing = !showing;
}

void Console::processCmd() {

    //convert to C++ string so we can use higher level tools
    std::string s(buffer);
    s.append(" ");

    std::cout << s << std::endl;

    //grab first word aka the cmd being ran
    std::string cmd = eatWord(s);

    //teleport logic
    if( cmd == "teleport" ) {

        if(s.length() < 6) {
            std::cout << "ILLEGAL TELEPORT COMMAND" << std::endl;
            return;
        }

        double x,y,z;
        std::string arg1 = eatWord(s);
        if(arg1 == "~")
            x = currPlayer->eye.x;
        else
            x = stod(arg1);

        std::string arg2 = eatWord(s);
        if(arg2 == "~")
            y = currPlayer->eye.y;
        else
            y = stod(arg2);

        std::string arg3 = eatWord(s);
        if(arg3 == "~")
            z = currPlayer->eye.z;
        else
            z = stod(arg3);

        currPlayer->eye = glm::vec3(x, y, z);
    } else if( cmd == "donut" ) {
        Brick b;
        b.Donut();
        b.coordinates = currPlayer->eye;

        currDim->add(b);



    }
    

}

std::string Console::eatWord(std::string& src) {
    std::string firstWord;
    size_t space_pos = src.find(" ");

    //does exist?
    if (space_pos != std::string::npos) {
        firstWord = src.substr(0, space_pos);
        src = src.substr(space_pos + 1);
    }

    return firstWord;
}

void Console::render() {
    ImGui::SetNextWindowPos(ImVec2(0,100));
    ImGui::SetNextWindowSize(ImVec2(200,100));
    ImGui::Begin("console", &showing);
    ImGui::InputText("", buffer, IM_ARRAYSIZE(buffer));
    if(ImGui::Button("submit") ) { processCmd(); }
    ImGui::End();


}
