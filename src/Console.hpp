#pragma once

#include "imgui.h"
#include "Player.h"
#include "Dim.h"
#include "Brick.h"
#include <string>


class Console {
public:
    Console(Player* currPlayer, Dim* currDim) { this->currPlayer = currPlayer; this->currDim = currDim; };
    void toggle();
    void render();
private:
    void processCmd();
    bool showing = true;
    ImVec2 coords = ImVec2(000,000);
    ImVec2 size = ImVec2(200,300);
    char buffer[512] = "\0";
    Player* currPlayer;
    Dim* currDim;
    std::string eatWord(std::string& src);
};