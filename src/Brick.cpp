#include "Brick.h"

Brick::Brick() {
    //default construction

    mesh = Mesh::create().withCube().build();


    material = Shader::getStandardPBR()->createMaterial();
    material->setColor({0.07f,0.4f,0.0f,1.0f}); //green!
    material->setMetallicRoughness({.5f,.5f});

    coordinates = glm::vec3(0,0,0);
}

void Brick::Cube(float len) {
    mesh = Mesh::create().withCube(len).build();
}

void Brick::Donut() {
    mesh = Mesh::create().withTorus().build();
}