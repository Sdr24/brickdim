#pragma once

#include <glm/glm.hpp>


class Player {
public:
    Player() { eye = {0, 0, 0}; at = {1.0f, 1.0f, 1.0f}; }
    glm::vec3 eye;
    glm::vec3 at;
};