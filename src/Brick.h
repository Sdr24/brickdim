//a brick is a primitive object
//it has a mesh (vertices), coordinates, and a material

#pragma once

#include "sre/Material.hpp"
#include "sre/Texture.hpp"
#include "sre/Mesh.hpp"
#include "sre/Renderer.hpp"
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

using namespace sre;

class Brick {
public:
    Brick();
    void Cube(float len = 1.0f);
    void Donut();

    std::shared_ptr<Mesh> mesh;
    std::shared_ptr<Material> material;
    glm::vec3 coordinates;
    //later: euler angles
};