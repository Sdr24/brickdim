#pragma once

#include <iostream>
#include <vector>
#include <fstream>

#include "sre/Texture.hpp"
#include "sre/Renderer.hpp"
#include "sre/Material.hpp"
#include "sre/SDLRenderer.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <sre/Skybox.hpp>
#include <sre/Inspector.hpp>

#include "Console.hpp"
#include "Player.h"
#include "Brick.h"
#include "Dim.h"

using namespace sre;

class BrickDim {
public:
    BrickDim() : console(&freecam, &dim) {
        r.init();
        
                                    //fov, near, far
        camera.setPerspectiveProjection(80,0.1,100);


        material = Shader::getStandardPBR()->createMaterial();
        material->setColor({0.07f,0.4f,0.0f,1.0f});
        material->setMetallicRoughness({.5f,.5f});

        mesh = Mesh::create().withCube().build();
        worldLights.setAmbientLight({1.0,1.0,0.9});
        worldLights.addLight(Light::create().withPointLight({0, 3,0}).withColor({1,1,0.9}).withRange(20).build());
        worldLights.addLight(Light::create().withPointLight({3, 0,0}).withColor({1,1,0.9}).withRange(20).build());
        worldLights.addLight(Light::create().withPointLight({0,-3,0}).withColor({1,1,0.9}).withRange(20).build());
        worldLights.addLight(Light::create().withPointLight({-3,0,0}).withColor({1,1,0.95}).withRange(20).build());

        auto tex = Texture::create()
                .withFileCubemap("resources/skybox/right.jpg", Texture::CubemapSide::PositiveX)
                .withFileCubemap("resources/skybox/left.jpg", Texture::CubemapSide::NegativeX)
                .withFileCubemap("resources/skybox/top.jpg", Texture::CubemapSide::PositiveY)
                .withFileCubemap("resources/skybox/bottom.jpg", Texture::CubemapSide::NegativeY)
                .withFileCubemap("resources/skybox/front.jpg", Texture::CubemapSide::PositiveZ)
                .withFileCubemap("resources/skybox/back.jpg", Texture::CubemapSide::NegativeZ)
                .withWrapUV(Texture::Wrap::ClampToEdge)
                .build();

        skybox = Skybox::create();

        auto skyboxMaterial = Shader::getSkybox()->createMaterial();
        skyboxMaterial->setTexture(tex);
        skybox->setMaterial(skyboxMaterial);
        
        Brick b;
        b.Donut();
        b.coordinates = glm::vec3(3,3,3);
        dim.add(b);

        r.frameRender = [&](){
            render();
        };
        r.mouseEvent = [&](SDL_Event& event){
            if (event.type == SDL_MOUSEMOTION && isRClicking){
                float mouseSpeed = 1/150.0f;
                float rotateY = -event.motion.yrel*mouseSpeed;
                float rotateX = -event.motion.xrel*mouseSpeed;

                 freecam.at = glm::mat3( glm::rotate(rotateX, glm::vec3(0, 1, 0) )) * freecam.at;

                 glm::vec3 toRotateAround = glm::cross(freecam.at, glm::vec3(0, 1, 0));

                 freecam.at = glm::mat3( glm::rotate(rotateY, toRotateAround )) * freecam.at;

                 camera.lookAt(freecam.eye, freecam.eye+freecam.at, up);


            }
            else if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (event.button.button==SDL_BUTTON_RIGHT){
                    isRClicking = true;
                }
            }
            else if (event.type == SDL_MOUSEBUTTONUP) {
                if (event.button.button==SDL_BUTTON_RIGHT){
                    isRClicking = false;
                }
            }

            if(event.type == SDL_MOUSEWHEEL) {
                const float SCROLL_SPEED = 0.5;
                if(event.wheel.y > 0) // scroll up
                {
                    freecam.eye += SCROLL_SPEED * freecam.at;
                }
                else if(event.wheel.y < 0) // scroll down
                {
                    freecam.eye += -SCROLL_SPEED * freecam.at;
                }
                camera.lookAt(freecam.eye, freecam.eye+freecam.at, up);
                

            }

            // if (event.type == SDL_MOUSEBUTTONUP){
            //     if (event.button.button==SDL_BUTTON_RIGHT){
            //         showGui = true;
            //     }
            // }
        };

        r.keyEvent = [&](SDL_Event& event){
            const float MOVE_SPEED = 0.1;

            if (event.type == SDL_KEYDOWN){
                glm::vec3 movementDirection = glm::cross(freecam.at, glm::vec3(0, 1, 0));
                switch(event.key.keysym.sym) {
                    
                    case SDLK_UP:
                        freecam.eye += MOVE_SPEED * glm::vec3(0,1,0);
                        break;
                    case SDLK_DOWN:
                        freecam.eye += -MOVE_SPEED * glm::vec3(0,1,0);
                        break;
                    case SDLK_w:
                        freecam.eye += MOVE_SPEED * freecam.at;
                        break;
                    case SDLK_a:
                        freecam.eye += -MOVE_SPEED * movementDirection;
                        break;
                    case SDLK_s:
                        freecam.eye += -MOVE_SPEED * freecam.at;
                        break;
                    case SDLK_d:
                        freecam.eye += MOVE_SPEED * movementDirection;
                        break;
                }
                    camera.lookAt(freecam.eye, freecam.eye+freecam.at, up);
                    
            }

        };

        r.startEventLoop();
    }

    void render(){
        auto renderPass = RenderPass::create()
                .withCamera(camera)
                .withWorldLights(&worldLights)
                .withSkybox(skybox)
                .withName("Frame")
                .build();

        renderPass.draw(mesh, glm::mat4(1), material);
        renderPass.draw(mesh, glm::translate( glm::vec3(2,2,2)) * glm::scale(glm::vec3(0.1)), material);
        renderPass.draw(mesh, glm::translate(glm::vec3(-2,-2,-2)) * glm::scale(glm::vec3(0.1)), material);

        //render da whole world
        for(Brick b : dim.bricks)
            renderPass.draw(b.mesh, glm::translate( b.coordinates ), b.material);


        static Inspector inspector;
        inspector.update();
        if (showGui) inspector.gui();


        //debug window
        ImGui::SetNextWindowPos(ImVec2(000,000));
        ImGui::SetNextWindowSize(ImVec2(200,100));
        ImGui::Begin("debug", nullptr, ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
        ImGui::Text("pos\t\t\tlook\nX: %f\t%f\nY: %f\t%f\nZ: %f\t%f", freecam.eye.x, freecam.at.x, freecam.eye.y, freecam.at.y, freecam.eye.z, freecam.at.z);
        ImGui::End();

        console.render();
        camera.lookAt(freecam.eye, freecam.eye+freecam.at, up);
        

    }
private:
    SDLRenderer r;
    Camera camera;
    WorldLights worldLights;
    std::shared_ptr<Mesh> mesh;
    std::shared_ptr<Material> material;
    std::shared_ptr<Skybox> skybox;
    bool showGui = false;
    glm::vec3 up = {0, 1, 0};
    bool isRClicking = false;
    Dim dim;

    Player freecam;
    Console console;
};